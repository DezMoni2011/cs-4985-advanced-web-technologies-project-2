﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Home.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Let's Start Managing...</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <div id="buttons">
        <asp:Button ID="btnViewCustomer" runat="server" Text="View Customer Info" OnClick="btnViewCustomer_Click" />
        <br />
        <br/>
        <asp:Button ID="btnSeeFeedback" runat="server" Text="See Customer Feedback" OnClick="btnSeeFeedback_Click" />
    </div>
    
     <p>
        View the contact information and details for any customer here.
    </p>
    
    <p>
        Check any feedback for any customer here.
    </p>
</asp:Content>

